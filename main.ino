int onBoardLED = D7;

void setup() {
    pinMode(onBoardLED, OUTPUT);
}

void loop() {
    //Read LUX value
    int lux = analogRead(A0);
    
    //Turning onboard led before publishing event value
    digitalWrite(onBoardLED, HIGH);
    Particle.publish("light_sensor", String(lux), PRIVATE);
    delay(30000);
    
    digitalWrite(onBoardLED, LOW);
    delay(30000);
}
